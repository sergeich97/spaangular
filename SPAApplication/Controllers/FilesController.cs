﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SPAApplication.Models;

namespace SPAApplication.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class UploadedFilesController : ControllerBase
    {
        private readonly SPAAppContext _context;

        public UploadedFilesController(SPAAppContext context)
        {
            _context = context;
        }

        // GET: api/UploadedFiles
        [HttpGet]
        
        public async Task<List<IGrouping<int,UploadedFile>>> GetUploadedFile()
        {
            var user = await _context.User.FirstOrDefaultAsync(u => u.Username == User.Identity.Name);
            var tt = await _context.UserFile.Where(uf => uf.UserId == user.Id).Select(f => f.FileId).ToArrayAsync();
            //var res = _context.UploadedFile.Where(f => tt.Contains(f.Id) == true).ToListAsync();
            var files = _context.User.Include(uf => uf.UserFile).ThenInclude(f => f.File).FirstOrDefault(u => u.Username == User.Identity.Name).UserFile.Select(uf => uf.File).GroupBy(f => f.UploadDate.Day).ToList();
            //var tn = files.GroupBy(f => f.UploadDate.Date).ToList();
            return files;
        }

        // GET: api/UploadedFiles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<UploadedFile>> GetUploadedFile(string id)
        {
            var UploadedFile = await _context.UploadedFile.Include(r => r.IdNavigation).FirstOrDefaultAsync(f => f.Id == id);
            var user = await _context.User.Include(uf=>uf.UserFile).FirstOrDefaultAsync(u => u.Username == User.Identity.Name);
            if (UploadedFile == null || UploadedFile.IdNavigation == null || user.UserFile.Select(u=>u.FileId).Contains(id)==false)
            {
                return NotFound();
            }

            return File(UploadedFile.IdNavigation.RawData, "application/octet-stream", UploadedFile.Name + '.' + UploadedFile.Extension);

        }
        
        // POST: api/UploadedFiles
        [HttpPost, DisableRequestSizeLimit]
        public async Task<ActionResult<UploadedFile>> PostUploadedFile()
        {            
            try
            {
                var user = await _context.User.FirstOrDefaultAsync(u => u.Username == User.Identity.Name);
                var UploadedFiles = Request.Form.Files;
                foreach (var file in UploadedFiles)
                {
                    if (file.Length>0)
                    {
                        var DbFile = new UploadedFile(ShortGuid.NewShortGuid().Value);
                        DbFile.Name = file.FileName;
                        var temp = DbFile.Name.Split('.').Where(d => d != "").ToArray();
                        DbFile.Extension = temp.Last();
                        DbFile.UploadDate = DateTime.Now.ToLocalTime();
                        DbFile.Hash = GetHashSHA256(file);
                        DbFile.Length = (int)file.Length;
                        if (_context.UploadedFile.Count(d => d.Hash.Equals(DbFile.Hash) == true) > 0)
                        {
                            var t = await _context.UploadedFile.FirstOrDefaultAsync(f => f.Hash.Equals(DbFile.Hash));
                            _context.UserFile.Add(new UserFile { UserId = user.Id, FileId = t.Id });
                            
                        }
                        else
                        {
                            var rawfile = new RawFile();
                            var stream = new MemoryStream();
                            file.OpenReadStream().CopyTo(stream);
                            rawfile.RawData = stream.ToArray();

                            rawfile.Id = DbFile.Id;
                            _context.UploadedFile.Add(DbFile);
                            _context.RawFile.Add(rawfile);

                            await _context.SaveChangesAsync();
                            _context.UserFile.Add(new UserFile { UserId = user.Id, FileId = DbFile.Id });

                            
                        }
                    }
                }

                await _context.SaveChangesAsync();

                return StatusCode(200);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
                
            }           
        }

        // DELETE: api/UploadedFiles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<UploadedFile>> DeleteUploadedFile(string id)
        {
            var UploadedFile = await _context.UploadedFile.FindAsync(id);
            //var raw = await _context.RawFile.FindAsync(id);
            //var user = await _context.User.FirstOrDefaultAsync(u => u.Username == User.Identity.Name);
            var user = await _context.User.Include(uf => uf.UserFile).FirstOrDefaultAsync(u => u.Username == User.Identity.Name);
            user.UserFile.Remove(user.UserFile.FirstOrDefault(uf => uf.FileId == id));
            if (UploadedFile==null)
            {
                return NotFound();
            }
            await _context.SaveChangesAsync();
            if (_context.UploadedFile.Include(uf => uf.UserFile).FirstOrDefault(f => f.Id == id).UserFile.Count == 0)
            {
                _context.UploadedFile.Remove(UploadedFile);

            }
            await _context.SaveChangesAsync();

            return UploadedFile;
        }


        private string GetHashSHA256(IFormFile file)
        {
            MemoryStream stream = new MemoryStream();
            file.OpenReadStream().CopyTo(stream);
            var bytes = SHA256.Create().ComputeHash(stream.ToArray());
            return BitConverter.ToString(bytes).Replace("-", String.Empty).ToLower();
        }
    }
}
