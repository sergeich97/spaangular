﻿using System;
using System.Collections.Generic;

namespace SPAApplication.Models
{
    public partial class UploadedFile
    {
        public UploadedFile(string id)
        {
            UserFile = new HashSet<UserFile>();
            this.Id = id;
        }

        public string Id { get; set; }
        public string Extension { get; set; }
        public string Hash { get; set; }
        public string Name { get; set; }
        public DateTime UploadDate { get; set; }
        public int Length { get; set; }

        public virtual RawFile IdNavigation { get; set; }
        public virtual ICollection<UserFile> UserFile { get; set; }
    }
}
