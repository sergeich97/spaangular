﻿using System;
using System.Collections.Generic;

namespace SPAApplication.Models
{
    public partial class RawFile
    {
        public string Id { get; set; }
        public byte[] RawData { get; set; }

        public virtual UploadedFile UploadedFile { get; set; }
    }
}
