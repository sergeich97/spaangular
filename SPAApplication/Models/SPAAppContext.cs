﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SPAApplication.Models
{
    public partial class SPAAppContext : DbContext
    {
        public SPAAppContext()
        {
        }

        public SPAAppContext(DbContextOptions<SPAAppContext> options)
            : base(options)
        {
        }

        public virtual DbSet<RawFile> RawFile { get; set; }
        public virtual DbSet<UploadedFile> UploadedFile { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserFile> UserFile { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=SpaApp;Username=postgres;Password=1");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("adminpack")
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<RawFile>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.RawData).IsRequired();
            });

            modelBuilder.Entity<UploadedFile>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Extension).IsRequired();

                entity.Property(e => e.Hash).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.UploadDate).HasColumnType("date");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.UploadedFile)
                    .HasForeignKey<UploadedFile>(d => d.Id)
                    .HasConstraintName("UploadedFile_fk");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Username).IsRequired();
            });

            modelBuilder.Entity<UserFile>(entity =>
            {
                entity.HasKey(e => new { e.FileId, e.UserId })
                    .HasName("UserFile_pkey");

                entity.HasOne(d => d.File)
                    .WithMany(p => p.UserFile)
                    .HasForeignKey(d => d.FileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("UserFile_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserFile)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("UserFile_fk1");
            });
        }
    }
}
