﻿using System;
using System.Collections.Generic;

namespace SPAApplication.Models
{
    public partial class UserFile
    {
        public string FileId { get; set; }
        public string UserId { get; set; }

        public virtual UploadedFile File { get; set; }
        public virtual User User { get; set; }
    }
}
