﻿using System;
using System.Collections.Generic;

namespace SPAApplication.Models
{
    public partial class User
    {
        public User()
        {
            UserFile = new HashSet<UserFile>();
        }

        public string Username { get; set; }
        public string Id { get; set; }

        public virtual ICollection<UserFile> UserFile { get; set; }
    }
}
