import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthService {

  constructor(private http:HttpClient) { }

  getUser():Observable<string>{
    return this.http.get("https://localhost:44369/api" + '/users/getuser',{responseType:'text'})
  }
}
