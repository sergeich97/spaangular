import { Injectable } from '@angular/core';
import { MyFile } from './file.model';
import {HttpClient} from "@angular/common/http"

@Injectable()
export class FileService {
  formData:MyFile;
  list: MyFile[];
  readonly rootUrl = "https://localhost:44369/api"
  constructor(private http:HttpClient) { }

  postFile(formData:FormData)
  {
      return this.http.post(this.rootUrl + '/Uploadedfiles',formData)
  }

  getAllFiles(){
    this.http.get(this.rootUrl+'/UploadedFiles').toPromise().then((res)=>{
      this.list = res as MyFile[];
      console.log(this.list);
    });
  }

  removeFile(id){
    return this.http.delete(this.rootUrl+'/UploadedFiles/'+id);
  }

  downloadFile(id){
    return this.http.get(this.rootUrl + '/UploadedFiles/'+id,{responseType:"blob"})   
  }
}
