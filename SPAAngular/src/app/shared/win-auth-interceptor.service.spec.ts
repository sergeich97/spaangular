import { TestBed, inject } from '@angular/core/testing';

import { WinAuthInterceptorService } from './win-auth-interceptor.service';

describe('WinAuthInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WinAuthInterceptorService]
    });
  });

  it('should be created', inject([WinAuthInterceptorService], (service: WinAuthInterceptorService) => {
    expect(service).toBeTruthy();
  }));
});
