export class MyFile {
    Id:string;
    Name:string;
    Hash:string;
    UploadDate:Date;
    Path:string;
    Length:number;
}
