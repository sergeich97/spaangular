import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from "@angular/common/http"


import {BrowserAnimationsModule} from "@angular/platform-browser/animations"
import { AppComponent } from './app.component';
import { FileListComponent } from './file-list/file-list.component';
import { UserComponent } from './user/user.component';
import { FileService } from './shared/file.service';
import { AddFileComponent } from './add-file/add-file.component';
import { WinAuthInterceptorService } from './shared/win-auth-interceptor.service';
import { AuthService } from './shared/auth.service';
import {MatExpansionModule} from "@angular/material"
import {MatDividerModule} from "@angular/material/divider"

@NgModule({
  declarations: [
    AppComponent,
    FileListComponent,
    UserComponent,
    AddFileComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    MatExpansionModule,
    MatDividerModule
  ],
  providers: [FileService,
  {
    provide:HTTP_INTERCEPTORS,
    useClass:WinAuthInterceptorService,
    multi:true
  },
AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
