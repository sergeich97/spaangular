import { Component, OnInit, ViewChild } from '@angular/core';
import { MyFile } from '../shared/file.model';
import { FileService } from '../shared/file.service';
import { saveAs} from 'file-saver'


@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit {

  public Xpand=true;
  public Button = "Раскрыть все";
  constructor(private service: FileService) { }

  ngOnInit() {
    this.service.getAllFiles();

  }

  deleteFile(id) {
    if (confirm('Do you want to remove file?')) {
      this.service.removeFile(id).subscribe((res) => {
        this.service.getAllFiles();
      }, (err) => {
        console.log(err);
      })
    }
  }
  download(id, name) {
    this.service.downloadFile(id).subscribe((res)=>{
      saveAs(res, name.trim());
    });
  }



}
