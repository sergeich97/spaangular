import { Component, OnInit } from '@angular/core';
import { MyFile } from '../shared/file.model';
import * as shajs from 'sha.js';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { FileService } from '../shared/file.service';

@Component({
  selector: 'app-add-file',
  templateUrl: './add-file.component.html',
  styleUrls: ['./add-file.component.css']
})
export class AddFileComponent implements OnInit {

  public progress:number;
  public message: string;
  constructor(private service:FileService) { }

  ngOnInit() {
  }

  upload(files)
  {
    
    const formData = new FormData();
    for (let file of files)
    formData.append(file.name, file);
    this.service.postFile(formData).subscribe((res)=>{ this.service.getAllFiles();},(err)=>{
      console.log(err);
    });



  }

}
