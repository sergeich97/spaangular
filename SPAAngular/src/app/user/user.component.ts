import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  WinUser } from '../shared/user.model';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private http:HttpClient) { }

  User:WinUser;
  ngOnInit() {
    this.getUser();
  }

  getUser(){
    this.http.get("https://localhost:44369/api"+ '/Users').subscribe((res)=>{
      this.User = res as WinUser;
      console.log(res);
    },(err)=>{
      console.log(err)
    })
  }

  // createUser(){
  //   this.http.post("https://localhost:44369/api"+ '/Users',)
  // }
}
