﻿using System.Data.SqlClient;
using System.IO;

namespace CreateDatabase
{
    public class Program
    {
        static void Main(string[] args)
        {
            var conn = new SqlConnection("Server=DESKTOP-1FP8T5V\\SQLEXPRESS;database=master;Integrated security=SSPI;");
            StreamReader read = new StreamReader("../../User.sql");
            var user = read.ReadToEnd();
            read.Close();
            read = new StreamReader("../../UploadedFile.sql");
            var uploaded = read.ReadToEnd();
            read.Close();
            read = new StreamReader("../../RawFile.sql");
            var raw = read.ReadToEnd();
            read.Close();
            read = new StreamReader("../../UserFile.sql");
            var userfile = read.ReadToEnd();
            read.Close();
            var table = "Create database SPAApp;";
            SqlCommand com = new SqlCommand(table, conn);
            conn.Open();
            com.ExecuteNonQuery();
            conn.Close();
            conn = new SqlConnection("Server=DESKTOP-1FP8T5V\\SQLEXPRESS;database=SPAApp;Integrated security=SSPI;");
            conn.Open();
            com = new SqlCommand(user, conn);
            com.ExecuteNonQuery();
            com = new SqlCommand(uploaded, conn);
            com.ExecuteNonQuery();
            com = new SqlCommand(raw, conn);
            com.ExecuteNonQuery();
            com = new SqlCommand(userfile, conn);
            com.ExecuteNonQuery();
            conn.Close();

        }
    }
}
