
CREATE TABLE [dbo].[UserFile](
	[FileId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
 CONSTRAINT [PK_UserFile] PRIMARY KEY CLUSTERED 
(
	[FileId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[UserFile]  WITH CHECK ADD  CONSTRAINT [FK_UserFile_File] FOREIGN KEY([FileId])
REFERENCES [dbo].[UploadedFile] ([Id])
ON UPDATE CASCADE


ALTER TABLE [dbo].[UserFile] CHECK CONSTRAINT [FK_UserFile_File]


ALTER TABLE [dbo].[UserFile]  WITH CHECK ADD  CONSTRAINT [FK_UserFile_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([Id])
ON UPDATE CASCADE


ALTER TABLE [dbo].[UserFile] CHECK CONSTRAINT [FK_UserFile_User]



